FROM arm64v8/debian:buster-slim
COPY 01_nodoc /etc/dpkg/dpkg.cfg.d/
COPY 99synaptics /etc/apt/apt.conf.d/
RUN apt-get update \
    && apt-get -y --no-install-recommends install virtualenv python3 python3-dev python3-cairo-dev libcairo2-dev python3-virtualenv python3-simplejson python3-gst-1.0 gstreamer1.0-plugins-good gstreamer1.0-plugins-base gstreamer1.0-plugins-bad gstreamer1.0-plugins-ugly gstreamer1.0-libav gstreamer1.0-alsa gstreamer1.0-tools libgirepository1.0-dev python3-prometheus-client python3-serial fonts-liberation v4l-utils alsa-utils \
    && apt clean && rm -rf /var/lib/apt/lists/*
